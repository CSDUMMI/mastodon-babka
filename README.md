#  Mastodon +Babka
Mastodon +Babka is a fork of Mastodon +Glitch that runs on the [`babka.social`](https://babka.social) instance.

`babka.social` uses an identity provider to manage user accounts and credentials and connects them with Mastodon through the OpenID Connect protocol. We have made a lot of changes to remove bugs with the OpenID Connect integration of Mastodon.

## Contributing
See [CHANGELOG-BABKA](./CHANGELOG-BABKA.md) for a description of all babka-specific changes made to Mastodon and Mastodon +Glitch.
